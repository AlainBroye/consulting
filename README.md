# Alain BROYE Consulting
  
Dossier professionnel STUDI

Je dois réaliser un site web dynamique de type CMS pour un cabinet de consulting en Smart City.

Le but de ce site est de présenter l'activité ainsi que les services proposés à la clientèle.

## 🛠️ Environnement technique serveur 

Configuration serveur d’hébergement (Auto-hébergé à mon domicile)

Hardware :<br>
[Raspberry Pi 4 8Go](https://www.raspberrypi.com/products/raspberry-pi-4-model-b/)

Système exploitation :<br>
[Ubuntu 22.04.1 LTS aarch64(Py3.7.8)](https://ubuntu.com/)

Software :<br>
[Server web (Apache 2.4.54)](https://httpd.apache.org/)<br>
[Moteur PHP (PHP 8.1.13)](https://www.php.net/)<br>
Extensions installées<br>
- fileinfo
- opcache
- redis
- imagemagick
- exif
- ldap

[Serveur de base de données (MySQL 8.0.24)](https://www.mysql.com/)<br>
[Gestion BDD (phpMyAdmin 5.2)](https://www.phpmyadmin.net/)<br>
[Serveur FTP (PureFTPD 1.0.49)](https://www.pureftpd.org/project/pure-ftpd/)<br>
[Serveur mail (Postfix 3.7.3)](https://www.postfix.org/)

Gestion serveur :<br>
[aaPanel 6.8.27](https://www.aapanel.com/new/index.html)

Sécurité :<br>
[Certificat SSL LetsEncrypt](https://letsencrypt.org/)<br>
[Gestion des DNS Cloudflare](https://www.cloudflare.com/)

Protection contre les attaques :<br>
[Cloudflare](https://www.cloudflare.com/)

Gestion des redirections HTTPS :<br>
[Cloudflare](https://www.cloudflare.com/)

Statistiques de fréquentation :<br>
[Google search console](https://search.google.com/)<br>
[Cloudflare](https://www.cloudflare.com/)


## 🖥️ Technologies front-office
- HTML5
- CSS3
- Bootstrap 5.3.0
- Font Awesome 6.3.0
- Javascript
- JQuery UI 1.13.2
- Laravel 9

## 🔐 Technologies back-office
- HTML5
- CSS3
- Bootstrap 5.3.0
- Font Awesome 6.3.0
- Javascript
- JQuery UI 1.13.2
- Laravel 9

## Outils de développement Offline

Ordinateurs :

Apple MacBooKAir M1<br>
Système d’exploitation (MacOS Monterey 12.3.1)

Périphériques mobiles :

Apple iPad 8 (iOS 15.4)<br>
Apple iPhone 12 (iOS 15.4.1)

Logiciels :

Visual Studio Code<br>
Photoshop<br>
Mamp web server<br>
Mozilla FileZilla<br>
Google Chrome<br>
MySQL Workbench


## 🙇 Auteur
#### Alain BROYE
- Github : [@ABroye](https://github.com/ABroye)
- Gitlab : [@ABroye](https://gitlab.com/AlainBroye)
- LinkedIn : [Alain BROYE](https://www.linkedin.com/in/alain-b-5bb38845/)

        


        